## Contributors: 
Team Back-end (team 2 @ [Telerik Academy Alpha JavaScript August 2018](https://gitlab.com/TelerikAcademy/alpha-js-aug-18))

Team members: 
[Milen](https://my.telerikacademy.com/Users/Milen_Rabadjiev), 
[Niko](https://my.telerikacademy.com/Users/NikoPenev)

## Project Description:
A NestJS application for movie database back-end service.

## Project Purpose:
To simulate a real back-end service with various endpoints.

## Tools used:
- NestJS
- TypeScript
- Jest

## Project URL:
[Here](https://gitlab.com/milenrab97/movie-backend-project)

## Trello:
[Board](https://trello.com/b/Q2O1oWKs/back-end-project)
