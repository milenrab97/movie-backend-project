import { Controller, Get, Query, Post, UseGuards, Body, ValidationPipe, Put, Param, ParseIntPipe, Delete } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { RoleService } from '../common/core/role.service';
import { AddRoleDTO } from '../models/user/add-role.dto';
import { number } from 'joi';
import { RolesGuard, Roles } from '../common';

@Controller('role')
export class RoleController {
    constructor(
        private readonly roleService: RoleService,
    ) { }
    @Get()
    getAllRoles(@Query() query) {
        return this.roleService.getAll();
    }
    @Get(':id')
    getByID(@Param('id', new ParseIntPipe()) id: number) {
        return this.roleService.getByID(id);
    }

    @Post()
    @Roles('admin')
    @UseGuards(AuthGuard(), RolesGuard)
    addRole(@Body(new ValidationPipe({
        transform: true,
        whitelist: true,
    }))
    role: AddRoleDTO) {
        const resultedRole = this.roleService.addRole(role);
        return resultedRole;
    }

    @Put(':id')
    @Roles('admin')
    @UseGuards(AuthGuard(), RolesGuard)

    updateRole(
        @Body(new ValidationPipe({
            transform: true,
            whitelist: true,
        })) role: AddRoleDTO,
        @Param('id', new ParseIntPipe()) id: number,
    ) {
        return this.roleService.updateRole(id, role);
    }

    @Delete(':id')
    @Roles('admin')
    @UseGuards(AuthGuard(), RolesGuard)
    deleteRole(
        @Param('id', new ParseIntPipe()) id: number,
    ) {
        return this.roleService.delete(id);
    }

}
