import { AuthModule } from './../auth/auth.module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CoreModule } from 'src/common/core/core.module';
import { RoleController } from './role.controller';
import { Role } from '../data/entities/role.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Role]), CoreModule, AuthModule],
  providers: [],
  exports: [],
  controllers: [RoleController],
})
export class RoleModule { }