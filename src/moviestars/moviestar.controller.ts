import { AddMoviestarDTO } from './../models/user/add-moviestar.dto';
import { Controller, Get, Query, Post, UseGuards, Body, ValidationPipe, Put, Param, ParseIntPipe, Delete } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { MoviestarsService } from '../common/core/moviestars.service';
import { RolesGuard, Roles } from '../common';

@Controller('moviestars')
export class MoviestarControler {
    constructor(
        private readonly moviestarService: MoviestarsService,
    ) { }
    @Get()
    getMoviestar(@Query() query) {
        if (query.firstName) {
            return this.moviestarService.getByFirstName(query.firstName);
        }
        if (query.lastName) {
            return this.moviestarService.getByLastName(query.lastName);
        }

        return this.moviestarService.getAll();
    }

    @Get(':id')
    readMoviestar(@Param('id', new ParseIntPipe()) id: number) {
        return this.moviestarService.read(id);
    }

    @Post()
    @Roles('admin')
    @UseGuards(AuthGuard(), RolesGuard)
    addMoviestar(@Body(new ValidationPipe({
        whitelist: true,
        transform: true,
    }))
    moviestar: AddMoviestarDTO) {
        return this.moviestarService.addMoviestar(moviestar);
    }

    @Put(':id')
    @Roles('admin') // to remove user
    @UseGuards(AuthGuard(), RolesGuard)

    updateMoviestar(
        @Body(new ValidationPipe({
            transform: true,
            whitelist: true,
            skipMissingProperties: true,
        }))
        director: AddMoviestarDTO,
        @Param('id', new ParseIntPipe()) id: number) {
        return this.moviestarService.updateMoviestar(id, director);
    }

    @Delete(':id')
    @Roles('admin')
    @UseGuards(AuthGuard(), RolesGuard)
    deleteMoviestar(
        @Param('id', new ParseIntPipe()) id: number,
    ) {
        return this.moviestarService.delete(id);
    }
}
