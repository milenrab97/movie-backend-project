import { AuthModule } from './../auth/auth.module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CoreModule } from 'src/common/core/core.module';
import { Moviestar } from '../data/entities/moviestar.entity';
import { MoviestarControler } from './moviestar.controller';
import { MoviestarsService } from '../common/core/moviestars.service';

@Module({
  imports: [TypeOrmModule.forFeature([Moviestar]), CoreModule, AuthModule],
  providers: [MoviestarsService],
  exports: [],
  controllers: [MoviestarControler],
})
export class MoviestarModule { }