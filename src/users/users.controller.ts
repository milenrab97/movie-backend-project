import { AdminGuard } from './../common/guards/roles/admin.guard';
import { AuthGuard } from '@nestjs/passport';
import { Controller, Get, UseGuards, Query, Put, Param, ParseIntPipe } from '@nestjs/common';
import { UsersService } from './../common/core/users.service';
import { Roles } from 'src/common/decorators/roles.decorator';
import { RolesGuard } from 'src/common';
import { User } from 'src/data/entities/user.entity';
import { User as UserID } from 'src/common';

@Controller('users')
export class UsersController {

  constructor(
    private readonly usersService: UsersService,
  ) { }

  @Get()
  // @Roles('admin', 'user')
  @Roles('admin')
  @UseGuards(AuthGuard(), RolesGuard)
  all() {
    return this.usersService.getAll();
  }

  @Get('/whoami')
  @UseGuards(AuthGuard())
  getCurrentUser(
    @UserID('id') userId: number,
  ) {
    return this.usersService.read(userId, true);
  }

  @Get(':id')
  readMoviestar(@Param('id', new ParseIntPipe()) id: number) {
      return this.usersService.read(id);
  }

  @Put(':id/promote')
  @Roles('owner')
  @UseGuards(AuthGuard(), RolesGuard)
  promoteUser(
    @Param('id', new ParseIntPipe()) userId: number,
  ) {
    return this.usersService.promoteUser(userId);
  }
}
