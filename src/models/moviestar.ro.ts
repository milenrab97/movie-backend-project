import { Movie } from 'src/data/entities/movie.entity';

export class MoviestarRO {
    id: number;
    firstName: string;
    lastName: string;
    movies?: Movie[];
}
