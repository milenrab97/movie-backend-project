import { IsString, Length } from 'class-validator';

export class AddGenreDTO {
    @IsString()
    @Length(1, 255)
    genreName: string;
}
