import { IsString, IsNumber, IsArray, IsInt, Length } from 'class-validator';

export class AddMovieDTO {
    @IsString()
    @Length(1, 255)
    title: string;

    @IsNumber()
    year: number;

    @IsNumber()
    length: number;

    @IsString()
    @Length(0, 4999)
    resume: string;

    @IsInt()
    directorId: number;
    @IsArray()
    genres: number[];

    @IsArray()
    moviestars: number[];
}
