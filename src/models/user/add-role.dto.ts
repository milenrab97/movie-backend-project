import { IsString, Length } from 'class-validator';

export class AddRoleDTO {
    @IsString()
    @Length(1, 250)
    roleName: string;
}
