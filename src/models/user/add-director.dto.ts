import { IsString, IsNumber, Length } from 'class-validator';

export class AddDirectorDTO {
    @IsString()
    @Length(1, 50)
    firstName: string;

    @IsString()
    @Length(1, 50)
    lastName: string;

    @IsString()
    @Length(1, 15)
    gender: string;

    @IsNumber()
    networth: number;

}
