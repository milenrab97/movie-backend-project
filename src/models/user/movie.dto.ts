import { IsString, Length, Matches, IsOptional, IsEmail } from 'class-validator';

export class MovieDTO {

    title: string;

    id: number;
    resume: string;

    rating?: number;

    // isAdmin: boolean;
}
