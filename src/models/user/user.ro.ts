import { Movie } from 'src/data/entities/movie.entity';

export class UserRO {
    id: number;
    email: string;
    role: string;
    watchlist?: Movie[];
}
