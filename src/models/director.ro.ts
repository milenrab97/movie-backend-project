export class DirectorRO {
    id: number;
    firstName: string;
    lastName: string;
    gender: string;
    networth: number;
}