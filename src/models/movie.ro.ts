import { DirectorRO } from './director.ro';
import { MoviestarRO } from './moviestar.ro';
export class MovieRO {
    id: number;
    title: string;
    year: number;
    length: number;
    resume: string;
    genres: string[];
    moviestars: MoviestarRO[];
    director: DirectorRO;
    rating: number;
}
