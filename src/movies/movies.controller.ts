import { UsersService } from './../common/core/users.service';
import { AddMovieDTO } from './../models/user/add-movie.dto';
import { MovieDTO } from './../models/user/movie.dto';
import { Controller, Get, Query, Post, UseGuards, Body, ValidationPipe, UsePipes, ParseIntPipe, Param, Put, Delete } from '@nestjs/common';
import { MoviesService } from 'src/common/core/movies.service';
import { Roles, RolesGuard, User } from 'src/common';
import { AuthGuard } from '@nestjs/passport';
import { read } from 'fs';

@Controller('movies')
export class MoviesController {
    constructor(
        private readonly moviesService: MoviesService,
    ) { }
    @Get()
    getAllMovies(
        @Query('page') page: number,
        @Query('count') count: number,
        @Query('title') title: string,
    ) {
        if (title) {
            return this.moviesService.getByTitle(page, count, title);
        }

        return this.moviesService.getAll(page, count);
    }

    @Post()
    @Roles('admin')
    @UseGuards(AuthGuard(), RolesGuard)
    addMovie(@Body(new ValidationPipe({
        transform: true,
        whitelist: true,
    }))
    movie: AddMovieDTO) {
        return this.moviesService.addMovie(movie);
    }

/*     @Get('/watchlist')
    @Roles('user', 'admin')
    @UseGuards(AuthGuard(), RolesGuard)
    getWatchlist(
        @User('id') userId: number,
    ) {
        return this.moviesService.getWatchlist(userId);
    } */

    @Get(':id')
    readMovie(@Param('id', new ParseIntPipe()) id: number) {
        return this.moviesService.read(id);
    }

    @Put(':id')
    @Roles('admin')
    @UseGuards(AuthGuard(), RolesGuard)
    updateMovies(
        @Param('id', new ParseIntPipe()) id: number,
        @Body(new ValidationPipe({
            transform: true,
            whitelist: true,
            skipMissingProperties: true,
        })) movie: AddMovieDTO,
    ) {
        return this.moviesService.updateMovie(id, movie);
    }
    // TODO: put

    @Delete(':id')
    @Roles('admin')
    @UseGuards(AuthGuard(), RolesGuard)
    deleteMovie(
        @Param('id', new ParseIntPipe()) id: number) {
        return this.moviesService.deleteMovie(id);
    }

    @Post(':id/watch')
    @Roles('user', 'admin')
    @UseGuards(AuthGuard(), RolesGuard)
    addToWatchlist(
        @Param('id', new ParseIntPipe()) id: number,
        @User('id') user: number) {
        this.moviesService.addToWatchlist(id, user);
        return true;
    }

    @Delete(':id/watch')
    @Roles('user', 'admin')
    @UseGuards(AuthGuard(), RolesGuard)
    removeFromWatchlist(
        @Param('id', new ParseIntPipe()) id: number,
        @User('id') user: number) {
        this.moviesService.removeFromWatchlist(id, user);
        return true;
    }

    @Post(':id/rate/:rating')
    @Roles('user', 'admin')
    @UseGuards(AuthGuard(), RolesGuard)
    rateMovie(
        @Param('id', new ParseIntPipe()) id: number,
        @User('id') user: number,
        @Param('rating', new ParseIntPipe()) rating: number,
    ) {
        return this.moviesService.rateMovie(id, user, rating);
    }
}
