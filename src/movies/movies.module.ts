import { AuthModule } from './../auth/auth.module';
import { Module } from '@nestjs/common';
import { MoviesController } from './movies.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Movie } from 'src/data/entities/movie.entity';
import { CoreModule } from 'src/common/core/core.module';
import { User } from 'src/data/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Movie, User]), CoreModule, AuthModule],
  controllers: [MoviesController],
})
export class MoviesModule { }
