import { MoviestarRO } from './../../models/moviestar.ro';
import { Movie } from './movie.entity';
import { Column, PrimaryGeneratedColumn, Entity, ManyToMany, JoinTable } from 'typeorm';

@Entity({
    name: 'moviestars',
})
export class Moviestar {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @ManyToMany(type => Movie, movie => movie.moviestars)
    movies: Movie[];

    convertToRO(includeMovies: boolean = true): MoviestarRO {
        const { id, firstName, lastName } = this;

        const response: MoviestarRO = {
            id,
            firstName,
            lastName,
        };

        if (includeMovies) {
            response.movies = this.movies;
        }

        return response;
    }
}
