import { UserRO } from './../../models/user/user.ro';
import { Column, PrimaryGeneratedColumn, Entity, ManyToOne, ManyToMany, JoinTable, OneToMany } from 'typeorm';
import { IsEmail } from 'class-validator';
import { Role } from './role.entity';
import { Movie } from './movie.entity';
import { Vote } from './vote.entity';

@Entity({
  name: 'users',
})
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    unique: true,
  })
  @IsEmail()
  email: string;

  @Column()
  password: string;

  @ManyToOne(type => Role, role => role.users, {
    eager: true,
  })
  role: Role;

  @ManyToMany(type => Movie, {
    eager: true,
  })
  @JoinTable()
  watchlist: Movie[];

  @OneToMany(type => Vote, vote => vote.user)
  votes: Promise<Vote[]>;

  convertToRO(): UserRO {
    const { id, email } = this;
    const role: string = this.role.roleName;

    const response: UserRO = {
      id,
      email,
      role,
    };

    response.watchlist = this.watchlist;

    return response;
  }
}
