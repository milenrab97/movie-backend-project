import { GenreRO } from './../../models/genre.ro';
import { Movie } from './movie.entity';
import { Column, PrimaryGeneratedColumn, Entity, ManyToMany } from 'typeorm';
import { IsEmail, IsString } from 'class-validator';

@Entity({
    name: 'genres',
})
export class Genre {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        unique: true,
    })
    @IsString()
    genreName: string;

    convertToRO(): GenreRO {
        const { id, genreName } = this;
        const response: GenreRO = {
            id,
            genreName,
        };

        return response;
    }
}
