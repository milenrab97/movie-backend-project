import { User } from './user.entity';
import { Column, PrimaryGeneratedColumn, Entity, ManyToOne } from 'typeorm';
import { Movie } from './movie.entity';
import { IsNumber, IsInt, Min, Max } from 'class-validator';

@Entity({
    name: 'votes',
})
export class Vote {
    @PrimaryGeneratedColumn()
    id: number;

    @IsInt()
    @Min(1)
    @Max(10)
    @Column({
        nullable: false,
    })
    rating: number;
/* 
    @Column()
    userId: number;

    @Column()
    movieId: number; */
    @ManyToOne(type => User, user => user.votes)
    user: User;

    @ManyToOne(type => Movie, movie => movie.votes)
    movie: Movie;

}
