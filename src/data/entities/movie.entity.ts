import { Column, PrimaryGeneratedColumn, Entity, OneToMany, ManyToMany, JoinTable, ManyToOne } from 'typeorm';
import { Genre } from './genre.entity';
import { Moviestar } from './moviestar.entity';
import { Director } from './director.entity';
import { Vote } from './vote.entity';

@Entity({
    name: 'movies',
})
export class Movie {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        unique: true,
    })
    title: string;

    @Column()
    year: number;

    @Column()
    length: number;

    @Column( {
        length: '5000',
    })
    resume: string;

    @ManyToMany(type => Genre, {
        eager: true,
    })
    @JoinTable()
    genres: Genre[];

    @ManyToMany(type => Moviestar, moviestar => moviestar.movies, {
        eager: true,
    })
    @JoinTable()
    moviestars: Moviestar[];

    @ManyToOne(type => Director, director => director.movies, {
        eager: true,
    })
    director: Director;

    @OneToMany(type => Vote, vote => vote.movie, {
        eager: true,
    })
    votes: Vote[];
}
