import { Column, PrimaryGeneratedColumn, Entity, OneToMany } from 'typeorm';
import { Movie } from './movie.entity';
import { DirectorRO } from 'src/models/director.ro';

@Entity({
    name: 'director',
})
export class Director {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column()
    gender: string;

    @Column()
    networth: number;

    @OneToMany(type => Movie, movie => movie.director)
    movies: Movie[];

    convertToRO(): DirectorRO {
        const { id, firstName, lastName, gender, networth } = this;
        const response: DirectorRO = {
            id,
            firstName,
            lastName,
            gender,
            networth,
        };

        return response;
    }
}
