import {MigrationInterface, QueryRunner} from "typeorm";

export class ResumeLength50001546705511620 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `genres` ADD UNIQUE INDEX `IDX_8d492be5f49e0233f3567b5986` (`genreName`)");
        await queryRunner.query("ALTER TABLE `users` DROP FOREIGN KEY `FK_368e146b785b574f42ae9e53d5e`");
        await queryRunner.query("ALTER TABLE `users` CHANGE `roleId` `roleId` int NULL");
        await queryRunner.query("ALTER TABLE `votes` DROP FOREIGN KEY `FK_5169384e31d0989699a318f3ca4`");
        await queryRunner.query("ALTER TABLE `votes` DROP FOREIGN KEY `FK_b73b3ad76fa124740465d4990cf`");
        await queryRunner.query("ALTER TABLE `votes` CHANGE `userId` `userId` int NULL");
        await queryRunner.query("ALTER TABLE `votes` CHANGE `movieId` `movieId` int NULL");
        await queryRunner.query("ALTER TABLE `movies` DROP FOREIGN KEY `FK_76fd356fe117a34a58cbccfb854`");
        await queryRunner.query("ALTER TABLE `movies` ADD UNIQUE INDEX `IDX_5aa0bbd146c0082d3fc5a0ad5d` (`title`)");
        await queryRunner.query("ALTER TABLE `movies` DROP COLUMN `resume`");
        await queryRunner.query("ALTER TABLE `movies` ADD `resume` varchar(5000) NOT NULL");
        await queryRunner.query("ALTER TABLE `movies` CHANGE `directorId` `directorId` int NULL");
        await queryRunner.query("ALTER TABLE `users` ADD CONSTRAINT `FK_368e146b785b574f42ae9e53d5e` FOREIGN KEY (`roleId`) REFERENCES `roles`(`id`)");
        await queryRunner.query("ALTER TABLE `votes` ADD CONSTRAINT `FK_5169384e31d0989699a318f3ca4` FOREIGN KEY (`userId`) REFERENCES `users`(`id`)");
        await queryRunner.query("ALTER TABLE `votes` ADD CONSTRAINT `FK_b73b3ad76fa124740465d4990cf` FOREIGN KEY (`movieId`) REFERENCES `movies`(`id`)");
        await queryRunner.query("ALTER TABLE `movies` ADD CONSTRAINT `FK_76fd356fe117a34a58cbccfb854` FOREIGN KEY (`directorId`) REFERENCES `director`(`id`)");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `movies` DROP FOREIGN KEY `FK_76fd356fe117a34a58cbccfb854`");
        await queryRunner.query("ALTER TABLE `votes` DROP FOREIGN KEY `FK_b73b3ad76fa124740465d4990cf`");
        await queryRunner.query("ALTER TABLE `votes` DROP FOREIGN KEY `FK_5169384e31d0989699a318f3ca4`");
        await queryRunner.query("ALTER TABLE `users` DROP FOREIGN KEY `FK_368e146b785b574f42ae9e53d5e`");
        await queryRunner.query("ALTER TABLE `movies` CHANGE `directorId` `directorId` int NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `movies` DROP COLUMN `resume`");
        await queryRunner.query("ALTER TABLE `movies` ADD `resume` varchar(255) NOT NULL");
        await queryRunner.query("ALTER TABLE `movies` DROP INDEX `IDX_5aa0bbd146c0082d3fc5a0ad5d`");
        await queryRunner.query("ALTER TABLE `movies` ADD CONSTRAINT `FK_76fd356fe117a34a58cbccfb854` FOREIGN KEY (`directorId`) REFERENCES `director`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");
        await queryRunner.query("ALTER TABLE `votes` CHANGE `movieId` `movieId` int NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `votes` CHANGE `userId` `userId` int NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `votes` ADD CONSTRAINT `FK_b73b3ad76fa124740465d4990cf` FOREIGN KEY (`movieId`) REFERENCES `movies`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");
        await queryRunner.query("ALTER TABLE `votes` ADD CONSTRAINT `FK_5169384e31d0989699a318f3ca4` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");
        await queryRunner.query("ALTER TABLE `users` CHANGE `roleId` `roleId` int NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `users` ADD CONSTRAINT `FK_368e146b785b574f42ae9e53d5e` FOREIGN KEY (`roleId`, `roleId`) REFERENCES `roles`(`id`,`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");
        await queryRunner.query("ALTER TABLE `genres` DROP INDEX `IDX_8d492be5f49e0233f3567b5986`");
    }

}
