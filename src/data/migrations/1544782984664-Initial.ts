import {MigrationInterface, QueryRunner} from "typeorm";

export class Initial1544782984664 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `genres` (`id` int NOT NULL AUTO_INCREMENT, `genreName` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `moviestars` (`id` int NOT NULL AUTO_INCREMENT, `firstName` varchar(255) NOT NULL, `lastName` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `roles` (`id` int NOT NULL AUTO_INCREMENT, `roleName` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `users` (`id` int NOT NULL AUTO_INCREMENT, `email` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `roleId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `votes` (`id` int NOT NULL AUTO_INCREMENT, `rating` int NOT NULL, `userId` int NULL, `movieId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `movies` (`id` int NOT NULL AUTO_INCREMENT, `title` varchar(255) NOT NULL, `year` int NOT NULL, `length` int NOT NULL, `resume` varchar(255) NOT NULL, `directorId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `director` (`id` int NOT NULL AUTO_INCREMENT, `firstName` varchar(255) NOT NULL, `lastName` varchar(255) NOT NULL, `gender` varchar(255) NOT NULL, `networth` int NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `users_watchlist_movies` (`usersId` int NOT NULL, `moviesId` int NOT NULL, PRIMARY KEY (`usersId`, `moviesId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `movies_genres_genres` (`moviesId` int NOT NULL, `genresId` int NOT NULL, PRIMARY KEY (`moviesId`, `genresId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `movies_moviestars_moviestars` (`moviesId` int NOT NULL, `moviestarsId` int NOT NULL, PRIMARY KEY (`moviesId`, `moviestarsId`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `users` ADD CONSTRAINT `FK_368e146b785b574f42ae9e53d5e` FOREIGN KEY (`roleId`) REFERENCES `roles`(`id`)");
        await queryRunner.query("ALTER TABLE `votes` ADD CONSTRAINT `FK_5169384e31d0989699a318f3ca4` FOREIGN KEY (`userId`) REFERENCES `users`(`id`)");
        await queryRunner.query("ALTER TABLE `votes` ADD CONSTRAINT `FK_b73b3ad76fa124740465d4990cf` FOREIGN KEY (`movieId`) REFERENCES `movies`(`id`)");
        await queryRunner.query("ALTER TABLE `movies` ADD CONSTRAINT `FK_76fd356fe117a34a58cbccfb854` FOREIGN KEY (`directorId`) REFERENCES `director`(`id`)");
        await queryRunner.query("ALTER TABLE `users_watchlist_movies` ADD CONSTRAINT `FK_23e0c5ff73cbdec6f072ea10004` FOREIGN KEY (`usersId`) REFERENCES `users`(`id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `users_watchlist_movies` ADD CONSTRAINT `FK_61237cec8b0881e4debd568cd58` FOREIGN KEY (`moviesId`) REFERENCES `movies`(`id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `movies_genres_genres` ADD CONSTRAINT `FK_cb43556a8849221b82cd17461c8` FOREIGN KEY (`moviesId`) REFERENCES `movies`(`id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `movies_genres_genres` ADD CONSTRAINT `FK_ccf6c10277da37e9fc265863fab` FOREIGN KEY (`genresId`) REFERENCES `genres`(`id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `movies_moviestars_moviestars` ADD CONSTRAINT `FK_4ce8d810a0b79715c23ecdf63b3` FOREIGN KEY (`moviesId`) REFERENCES `movies`(`id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `movies_moviestars_moviestars` ADD CONSTRAINT `FK_38f973c3469d12bab3538b08ca8` FOREIGN KEY (`moviestarsId`) REFERENCES `moviestars`(`id`) ON DELETE CASCADE");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `movies_moviestars_moviestars` DROP FOREIGN KEY `FK_38f973c3469d12bab3538b08ca8`");
        await queryRunner.query("ALTER TABLE `movies_moviestars_moviestars` DROP FOREIGN KEY `FK_4ce8d810a0b79715c23ecdf63b3`");
        await queryRunner.query("ALTER TABLE `movies_genres_genres` DROP FOREIGN KEY `FK_ccf6c10277da37e9fc265863fab`");
        await queryRunner.query("ALTER TABLE `movies_genres_genres` DROP FOREIGN KEY `FK_cb43556a8849221b82cd17461c8`");
        await queryRunner.query("ALTER TABLE `users_watchlist_movies` DROP FOREIGN KEY `FK_61237cec8b0881e4debd568cd58`");
        await queryRunner.query("ALTER TABLE `users_watchlist_movies` DROP FOREIGN KEY `FK_23e0c5ff73cbdec6f072ea10004`");
        await queryRunner.query("ALTER TABLE `movies` DROP FOREIGN KEY `FK_76fd356fe117a34a58cbccfb854`");
        await queryRunner.query("ALTER TABLE `votes` DROP FOREIGN KEY `FK_b73b3ad76fa124740465d4990cf`");
        await queryRunner.query("ALTER TABLE `votes` DROP FOREIGN KEY `FK_5169384e31d0989699a318f3ca4`");
        await queryRunner.query("ALTER TABLE `users` DROP FOREIGN KEY `FK_368e146b785b574f42ae9e53d5e`");
        await queryRunner.query("DROP TABLE `movies_moviestars_moviestars`");
        await queryRunner.query("DROP TABLE `movies_genres_genres`");
        await queryRunner.query("DROP TABLE `users_watchlist_movies`");
        await queryRunner.query("DROP TABLE `director`");
        await queryRunner.query("DROP TABLE `movies`");
        await queryRunner.query("DROP TABLE `votes`");
        await queryRunner.query("DROP TABLE `users`");
        await queryRunner.query("DROP TABLE `roles`");
        await queryRunner.query("DROP TABLE `moviestars`");
        await queryRunner.query("DROP TABLE `genres`");
    }

}
