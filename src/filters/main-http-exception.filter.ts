
import { Catch, ExceptionFilter, HttpException, ArgumentsHost } from '@nestjs/common';
@Catch()
export class MainHttpExceptionFilter implements ExceptionFilter {
    catch(exception: any, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const request = ctx.getRequest();
        const response = ctx.getResponse();
        const status = exception.getStatus();

        response
            .status(status)
            .json({
                statusCode: status,
                timestamp: new Date().toISOString(),
                path: request.url,
                method: request.method,
                message: exception.message.message || exception.message,
            });
    }
}
