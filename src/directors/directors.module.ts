import { Director } from './../data/entities/director.entity';
import { DirectorController } from './directors.controller';
import { AuthModule } from './../auth/auth.module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CoreModule } from 'src/common/core/core.module';

@Module({
  imports: [TypeOrmModule.forFeature([Director]), CoreModule, AuthModule],
  providers: [],
  exports: [],
  controllers: [DirectorController],
})
export class DirectorModule { }