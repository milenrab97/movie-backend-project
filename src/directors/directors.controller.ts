import { AddDirectorDTO } from './../models/user/add-director.dto';
import { DirectorService } from './../common/core/director.service';
import { Controller, Get, Query, Post, UseGuards, Body, ValidationPipe, Put, Delete, Param, ParseIntPipe } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard, Roles } from '../common';

@Controller('director')
export class DirectorController {
    constructor(
        private readonly directorService: DirectorService,
    ) { }
    @Get()
    getDirectors(@Query() query) {
        if (query.id) {
            return this.directorService.read(query.id);
        }

        if (query.firstName) {
            return this.directorService.getByFirstName(query.firstName);
        }

        if (query.lastName) {
            return this.directorService.getByLastName(query.lastName);
        }

        return this.directorService.getAll();
    }

    @Get(':id')
    readDirector(@Param('id', new ParseIntPipe()) id: number) {
        return this.directorService.read(id);
    }

    @Post()
    @Roles('admin')
    @UseGuards(AuthGuard(), RolesGuard)
    async addDirector(@Body(new ValidationPipe({
        transform: true,
        whitelist: true,
    }))
    director: AddDirectorDTO) {
        const res = await this.directorService.addDirector(director);
        return res;
    }

    @Put(':id')
    @Roles('admin')
    @UseGuards(AuthGuard(), RolesGuard)

    updateDirector(
        @Param('id', new ParseIntPipe()) id: number,
        @Body(new ValidationPipe({
            transform: true,
            whitelist: true,
            skipMissingProperties: true,
        }))
        director: AddDirectorDTO,
    ) {
        return this.directorService.updateDirector(id, director);
    }

    @Delete(':id')
    @Roles('admin')
    @UseGuards(AuthGuard(), RolesGuard)
    deleteDirector(
        @Param('id', new ParseIntPipe()) id: number,
    ) {
        return this.directorService.deleteDirector(id);
    }
}
