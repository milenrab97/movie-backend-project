import { Director } from '../data/entities/director.entity';
import { AddDirectorDTO } from '../models/user/add-director.dto';
import { DirectorRO } from '../models/director.ro';
import { DirectorService } from '../common/core/director.service';
import { Test, TestingModule } from '@nestjs/testing';

describe('DirectorService', () => {
    let directorService: DirectorService;
    let directorEntity: Director = new Director();

    const directorRepository: any = {
        findOne: async () => {
            return directorEntity;
        },
        find: () => null,
        update: () => ({}),
        remove: () => ({}),
        save: async () => {
            return directorEntity;
        },
        create: () => ({}),
    };

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [DirectorService,
                        {
                            provide: 'DirectorRepository',
                            useValue: directorRepository,
                        },
            ],
        })
            // .overrideProvider(getRepositoryToken(Director))
            // .useValue(directorRepository)
            .compile();

        directorService = module.get<DirectorService>(DirectorService);
    });

    describe('read method ', () => {
        it('should return correct data', async () => {
            // Arrange
            directorEntity = new Director();
            directorEntity.id = 1;
            directorEntity.firstName = 'Steven';
            directorEntity.lastName = 'Spielberg';
            directorEntity.gender = 'male';
            directorEntity.networth = 100000;

            const directorRO: DirectorRO = new DirectorRO();
            directorRO.id = 1;
            directorRO.firstName = 'Steven';
            directorRO.lastName = 'Spielberg';
            directorRO.gender = 'male';
            directorRO.networth = 100000;

            jest.spyOn(directorService, 'read');

            // Act
            const result = await directorService.read(1);

            // Assert
            expect(result.id).toBe(directorRO.id);
            expect(result.firstName).toBe(directorRO.firstName);
            expect(result.lastName).toBe(directorRO.lastName);
        });

        it('should throw error when director is not found', async () => {
            // Arrange
            directorEntity = null;

            jest.spyOn(directorService, 'read');

            // Act & Assert
            let message = false;
            try {
                await directorService.read(1);
            } catch (err) {
                message = err.message;
            }
            expect(message).toBeTruthy();
        });

        it('should call directorRepository findOne method', async () => {
            // Arrange
            directorEntity = new Director();
            directorEntity.id = 3;
            directorEntity.firstName = 'Steven';
            directorEntity.lastName = 'Spielberg';

            const spy = jest.spyOn(directorRepository, 'findOne');

            // Act
            await directorService.read(1);

            // Assert
            expect(spy).toHaveBeenCalled();
        });
    });

    describe('addDirector method ', () => {
        let directorToAdd: AddDirectorDTO;
        it('should save and return correct data', async () => {
            // Arrange
            directorToAdd = new AddDirectorDTO();
            directorToAdd.firstName = 'Martin';
            directorToAdd.lastName = 'Scorsese';

            directorEntity = new Director();
            directorEntity.id = 1;
            directorEntity.firstName = 'Martin';
            directorEntity.lastName = 'Scorsese';

            directorRepository.findOne = () => {
                return null;
            };

            jest.spyOn(directorService, 'addDirector');

            // Act
            const result = await directorService.addDirector(directorToAdd);
            // Assert
            expect(result.firstName).toBe(directorToAdd.firstName);
            expect(result.lastName).toBe(directorToAdd.lastName);
        });

        it('should throw error when moviestar already exists', async () => {
                // Arrange
            directorToAdd = new AddDirectorDTO();
            directorToAdd.firstName = 'Martin';
            directorToAdd.lastName = 'Scorsese';
            directorEntity = new Director();
            directorEntity.id = 1;
            directorEntity.firstName = 'Martin';
            directorEntity.lastName = 'Scorsese';

            directorRepository.findOne = () => {
                return directorEntity;
            };

            jest.spyOn(directorService, 'addDirector');

            // Act & Assert
            let message = false;
            try {
                await directorService.addDirector(directorToAdd);
            } catch (err) {
                message = err.message;
            }
            expect(message).toBeTruthy();
        });

        it('should call directorRepository findOne method', async () => {
            // Arrange
            directorToAdd = new AddDirectorDTO();
            directorToAdd.firstName = 'Martin';
            directorToAdd.lastName = 'Scorsese';

            directorRepository.findOne = () => {
                return null;
            };

            const spy = jest.spyOn(directorRepository, 'findOne');

            // Act
            await directorService.addDirector(directorToAdd);

            // Assert
            expect(spy).toHaveBeenCalled();
            });

        it('should call directorRepository create method', async () => {
            // Arrange
            directorToAdd = new AddDirectorDTO();
            directorToAdd.firstName = 'Martin';
            directorToAdd.lastName = 'Scorsese';

            directorRepository.findOne = () => {
                return null;
            };

            const spy = jest.spyOn(directorRepository, 'create');

            // Act
            await directorService.addDirector(directorToAdd);

            // Assert
            expect(spy).toHaveBeenCalled();
        });

        it('should call moviestarRepository save method', async () => {
            // Arrange
            directorToAdd = new AddDirectorDTO();
            directorToAdd.firstName = 'Martin';
            directorToAdd.lastName = 'Scorsese';

            directorRepository.findOne = () => {
                return null;
            };

            const spy = jest.spyOn(directorRepository, 'save');

            // Act
            await directorService.addDirector(directorToAdd);

            // Assert
            expect(spy).toHaveBeenCalled();
        });

        describe('updateDirector method ', () => {

        it('should update and return correct data', async () => {
            // Arrange
            const updatingDirector: Partial<AddDirectorDTO> = new AddDirectorDTO();
            updatingDirector.firstName = 'Steven';

            directorEntity = new Director();
            directorEntity.id = 1;
            directorEntity.firstName = 'Martin';
            directorEntity.lastName = 'Scorsese';

            const updatedDirector = new Director();
            updatedDirector.id = directorEntity.id;
            updatedDirector.firstName = 'Steven';
            updatedDirector.lastName = directorEntity.lastName;

            directorRepository.findOne = () => {
                return directorEntity;
            };
            directorRepository.update = () => {
                directorRepository.findOne = () => {
                    return updatedDirector;
                };

                return updatedDirector;
            };

            // Act
            const result = await directorService.updateDirector(1, updatingDirector);

            // Assert
            expect(result.id).toBe(updatedDirector.id);
            expect(result.firstName).toBe(updatedDirector.firstName);
            expect(result.lastName).toBe(updatedDirector.lastName);
        });

        it('should call directorRepository findOne method twice', async () => {
            // Arrange
            const updatingDirector: Partial<AddDirectorDTO> = new AddDirectorDTO();
            updatingDirector.firstName = 'Steven';

            directorEntity = new Director();
            directorEntity.id = 1;
            directorEntity.firstName = 'Martin';
            directorEntity.lastName = 'Scorsese';

            directorRepository.findOne = () => {
                return directorEntity;
            };
            directorRepository.update = () => {
                return directorEntity;
            };

            const spy = jest.spyOn(directorRepository, 'findOne');

            // Act
            await directorService.updateDirector(1, updatingDirector);

            // Assert
            expect(spy).toHaveBeenCalledTimes(2);
        });

        describe('getByFirstName method', () => {
            it('should return director found', async () => {
                // Arrange
                const query: string = 'Steven';

                directorEntity = new Director();
                directorEntity.id = 1;
                directorEntity.firstName = 'Steven';
                directorEntity.lastName = 'Spielberg';

                directorRepository.find = () => {
                    return [directorEntity];
                };

                // Act & Assert
                expect(await directorService.getByFirstName(query)).toEqual([directorEntity]);
            });
        });

        describe('Delete method', () => {

            it('should return correct data', async () => {
                // Arrange
                const directorRO: DirectorRO = new DirectorRO();
                directorRO.id = 1;
                directorRO.firstName = 'Steven';
                directorRO.lastName = 'Spielberg';
                directorRO.gender = 'male';
                directorRO.networth = 100000;

                jest.spyOn(directorService, 'deleteDirector');

                // Act
                // const spy = jest.spyOn(directorRepository, 'remove');

                const result = await directorService.deleteDirector(1);

                // Assert
                // expect(spy).toHaveBeenCalled();
                expect(result.id).toBe(directorRO.id);
                expect(result.firstName).toBe(directorRO.firstName);
                expect(result.lastName).toBe(directorRO.lastName);
            });
            it('should throw error when director is not found', async () => {
                // Arrange
                directorEntity = null;

                jest.spyOn(directorService, 'deleteDirector');

                // Act & Assert
                let message = false;
                try {
                    await directorService.deleteDirector(1);
                } catch (err) {
                    message = err.message;
                }
                expect(message).toBeTruthy();
            });
            it('should call directorRepository remove method', async () => {
                // Arrange
                directorEntity = new Director();
                directorEntity.id = 1;
                directorEntity.firstName = 'Steven';
                directorEntity.lastName = 'Spielberg';

                const spy = jest.spyOn(directorRepository, 'remove');

                // Act
                await directorService.deleteDirector(1);

                // Assert
                expect(spy).toHaveBeenCalled();
            });
        });

    });
});
});