import { AddMovieDTO } from './../models/user/add-movie.dto';
import { TestingModule, Test } from '@nestjs/testing';
import { MoviesService } from './../common/core/movies.service';
import { Moviestar } from '../data/entities/moviestar.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Movie } from '../data/entities/movie.entity';
import { User } from '../data/entities/user.entity';
import { Director } from '../data/entities/director.entity';
import { Vote } from '../data/entities/vote.entity';
import { Genre } from '../data/entities/genre.entity';
import { Role } from '../data/entities/role.entity';
describe('MoviesService', () => {
    let moviesService: MoviesService;
    const movieRepository: any = {
        findOne: async () => {
            return null;
        },
        find: () => null,
        update: () => ({}),
        remove: () => ({}),
        save: async () => {
            return null;
        },
        create: () => ({}),
    };

    const genresRepository: any = {
        findOne: async () => {
            return null;
        },
        find: () => null,
        update: () => ({}),
        remove: () => ({}),
        save: async () => {
            return null;
        },
        create: () => ({}),
    };

    const votesRepository: any = {
        findOne: async () => {
            return null;
        },
        find: () => null,
        update: () => ({}),
        remove: () => ({}),
        save: async () => {
            return null;
        },
        create: () => ({}),
    };

    const directorRepository: any = {
        findOne: async () => {
            return null;
        },
        find: () => null,
        update: () => ({}),
        remove: () => ({}),
        save: async () => {
            return null;
        },
        create: () => ({}),
    };

    const userRepository: any = {
        findOne: async () => {
            return null;
        },
        find: () => null,
        update: () => ({}),
        remove: () => ({}),
        save: async () => {
            return null;
        },
        create: () => ({}),
    };

    const moviestarRepository: any = {
        findOne: async () => {
            return null;
        },
        find: () => null,
        update: () => ({}),
        remove: () => ({}),
        save: async () => {
            return null;
        },
        create: () => ({}),
    };

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [MoviesService,
                /*         {
                            provide: 'MoviestarRepository',
                            useValue: moviestarRepository,
                        }, */
            ],
        })
            .overrideProvider(getRepositoryToken(Moviestar))
            .useValue(moviestarRepository)
            .overrideProvider(getRepositoryToken(Movie))
            .useValue(movieRepository)
            .overrideProvider(getRepositoryToken(User))
            .useValue(userRepository)
            .overrideProvider(getRepositoryToken(Director))
            .useValue(directorRepository)
            .overrideProvider(getRepositoryToken(Vote))
            .useValue(votesRepository)
            .overrideProvider(getRepositoryToken(Genre))
            .useValue(genresRepository)
            .compile();

        moviesService = module.get<MoviesService>(MoviesService);
    });

    describe('read method ', () => {
        it('should return correct movie', async () => {
            // Arrange
            const director = new Director();
            director.firstName = 'Ivan';

            const movie = new Movie();
            movie.title = 'title';
            movie.director = director;
            movie.genres = [];
            movie.moviestars = [];

            movieRepository.findOne = () => {
                return movie;
            };

            // Act
            const result = await moviesService.read(1);

            // Assert
            expect(result.title).toBe(movie.title);
        });

        it('should throw error when movie is not found', async () => {
            // Arrange
            movieRepository.findOne = () => {
                return null;
            };

            // Act & Assert
            let message = false;
            try {
                await moviesService.read(1);
            } catch (err) {
                message = err.message;
            }
            expect(message).toBeTruthy();
        });

        it('should call movieRepository findOne method', async () => {
            // Arrange
            const director = new Director();
            director.firstName = 'Ivan';

            const movie = new Movie();
            movie.title = 'title';
            movie.director = director;
            movie.genres = [];
            movie.moviestars = [];

            movieRepository.findOne = () => {
                return movie;
            };
            const spy = jest.spyOn(movieRepository, 'findOne');

            // Act
            await moviesService.read(1);

            // Assert
            expect(spy).toHaveBeenCalled();
        });
    });

    describe('removeFromWatchlist method ', () => {
        describe('should throw ', () => {
            it('when movie is not found', async () => {
                // Arrange
                movieRepository.findOne = () => {
                    return null;
                };
                userRepository.findOne = () => {
                    return {};
                };

                // Act & Assert
                let message = false;
                try {
                    await moviesService.removeFromWatchlist(1, 1);
                } catch (err) {
                    message = err.message;
                }
                expect(message).toBeTruthy();
            });

            it('when user\'s watchlist is empty', async () => {
                // Arrange
                movieRepository.findOne = () => {
                    return {};
                };
                const user = new User();
                user.watchlist = [];
                userRepository.findOne = () => {
                    return user;
                };

                // Act & Assert
                let message = false;
                try {
                    await moviesService.removeFromWatchlist(1, 1);
                } catch (err) {
                    message = err.message;
                }
                expect(message).toBeTruthy();
            });

            it('when movie is not in user\'s watchlist', async () => {
                // Arrange
                const director = new Director();
                director.firstName = 'Ivan';

                const movie = new Movie();
                movie.id = 1;
                movie.title = 'title';
                movie.director = director;
                movie.genres = [];
                movie.moviestars = [];

                const movie1 = new Movie();
                movie.id = 2;
                movie.title = 'title';
                movie.director = director;
                movie.genres = [];
                movie.moviestars = [];

                movieRepository.findOne = () => {
                    return movie;
                };

                const user = new User();
                user.watchlist = [movie1];
                userRepository.findOne = () => {
                    return user;
                };

                // Act & Assert
                let message = false;
                try {
                    await moviesService.removeFromWatchlist(1, 1);
                } catch (err) {
                    message = err.message;
                }
                expect(message).toBeTruthy();
            });
        });

        it('should call movieRepository findOne method', async () => {
            // Arrange
            const director = new Director();
            director.firstName = 'Ivan';

            const movie = new Movie();
            movie.id = 1;
            movie.title = 'title';
            movie.director = director;
            movie.genres = [];
            movie.moviestars = [];

            movieRepository.findOne = () => {
                return movie;
            };

            const role = new Role();
            role.roleName = '';

            const user = new User();
            user.watchlist = [movie];
            user.role = role;
            userRepository.findOne = () => {
                return user;
            };

            const spy = jest.spyOn(movieRepository, 'findOne');

            // Act
            await moviesService.removeFromWatchlist(1, 1);

            // Assert
            expect(spy).toHaveBeenCalled();
        });

        it('should call userRepository findOne method', async () => {
            // Arrange
            const director = new Director();
            director.firstName = 'Ivan';

            const movie = new Movie();
            movie.id = 1;
            movie.title = 'title';
            movie.director = director;
            movie.genres = [];
            movie.moviestars = [];

            movieRepository.findOne = () => {
                return movie;
            };

            const role = new Role();
            role.roleName = '';

            const user = new User();
            user.watchlist = [movie];
            user.role = role;
            userRepository.findOne = () => {
                return user;
            };

            const spy = jest.spyOn(userRepository, 'findOne');

            // Act
            await moviesService.removeFromWatchlist(1, 1);

            // Assert
            expect(spy).toHaveBeenCalled();
        });

        it('should call userRepository findOne method', async () => {
            // Arrange
            const director = new Director();
            director.firstName = 'Ivan';

            const movie = new Movie();
            movie.id = 1;
            movie.title = 'title';
            movie.director = director;
            movie.genres = [];
            movie.moviestars = [];

            movieRepository.findOne = () => {
                return movie;
            };

            const role = new Role();
            role.roleName = '';

            const user = new User();
            user.watchlist = [movie];
            user.role = role;

            userRepository.findOne = () => {
                return user;
            };
            userRepository.save = () => {
                return user;
            };

            const spy = jest.spyOn(userRepository, 'save');

            // Act
            await moviesService.removeFromWatchlist(1, 1);

            // Assert
            expect(spy).toHaveBeenCalled();
        });
    });

    describe('rateMovie method', () => {
        it('should call votesRepository update method if user has already voted', async () => {
            // Arrange
            const director = new Director();
            director.firstName = 'Ivan';

            const movie = new Movie();
            movie.id = 1;
            movie.title = 'title';
            movie.director = director;
            movie.genres = [];
            movie.moviestars = [];

            movieRepository.findOne = () => {
                return movie;
            };

            const role = new Role();
            role.roleName = '';

            const user = new User();
            user.watchlist = [movie];
            user.role = role;

            userRepository.findOne = () => {
                return user;
            };

            votesRepository.findOne = () => {
                return {};
            };
            votesRepository.update = () => {
                return {};
            };

            const spy = jest.spyOn(votesRepository, 'update');

            // Act
            await moviesService.rateMovie(1, 1, 1);

            // Assert
            expect(spy).toHaveBeenCalled();
        });

        it('should call votesRepository create method if user has not voted for the movie', async () => {
            // Arrange
            const director = new Director();
            director.firstName = 'Ivan';

            const movie = new Movie();
            movie.id = 1;
            movie.title = 'title';
            movie.director = director;
            movie.genres = [];
            movie.moviestars = [];

            movieRepository.findOne = () => {
                return movie;
            };

            const role = new Role();
            role.roleName = '';

            const user = new User();
            user.watchlist = [movie];
            user.role = role;

            userRepository.findOne = () => {
                return user;
            };

            votesRepository.findOne = () => {
                return null;
            };
            votesRepository.create = () => {
                return {};
            };

            votesRepository.save = () => {
                return {};
            };

            const spy = jest.spyOn(votesRepository, 'create');

            // Act
            await moviesService.rateMovie(1, 1, 1);

            // Assert
            expect(spy).toHaveBeenCalled();
        });

        it('should call votesRepository save method if user has not voted for the movie', async () => {
            // Arrange
            const director = new Director();
            director.firstName = 'Ivan';

            const movie = new Movie();
            movie.id = 1;
            movie.title = 'title';
            movie.director = director;
            movie.genres = [];
            movie.moviestars = [];

            movieRepository.findOne = () => {
                return movie;
            };

            const role = new Role();
            role.roleName = '';

            const user = new User();
            user.watchlist = [movie];
            user.role = role;

            userRepository.findOne = () => {
                return user;
            };

            votesRepository.findOne = () => {
                return null;
            };
            votesRepository.create = () => {
                return {};
            };

            votesRepository.save = () => {
                return {};
            };

            const spy = jest.spyOn(votesRepository, 'save');

            // Act
            await moviesService.rateMovie(1, 1, 1);

            // Assert
            expect(spy).toHaveBeenCalled();
        });

        it('should throw if movie is not found', async () => {
            // Arrange
            const director = new Director();
            director.firstName = 'Ivan';

            const movie = new Movie();
            movie.id = 1;
            movie.title = 'title';
            movie.director = director;
            movie.genres = [];
            movie.moviestars = [];

            movieRepository.findOne = () => {
                return null;
            };

            const role = new Role();
            role.roleName = '';

            const user = new User();
            user.watchlist = [movie];
            user.role = role;

            userRepository.findOne = () => {
                return user;
            };

            // Act & Assert
            let message = false;
            try {
                await moviesService.rateMovie(1, 1, 1);
            } catch (err) {
                message = err.message;
            }
            expect(message).toBeTruthy();
        });

        it('should call movieRepository findOne method twice', async () => {
            // Arrange
            const director = new Director();
            director.firstName = 'Ivan';

            const movie = new Movie();
            movie.id = 1;
            movie.title = 'title';
            movie.director = director;
            movie.genres = [];
            movie.moviestars = [];

            movieRepository.findOne = () => {
                return movie;
            };

            const role = new Role();
            role.roleName = '';

            const user = new User();
            user.watchlist = [movie];
            user.role = role;

            userRepository.findOne = () => {
                return user;
            };

            votesRepository.findOne = () => {
                return null;
            };
            votesRepository.create = () => {
                return {};
            };

            votesRepository.save = () => {
                return {};
            };

            const spy = jest.spyOn(movieRepository, 'findOne');

            // Act
            await moviesService.rateMovie(1, 1, 1);

            // Assert
            expect(spy).toHaveBeenCalledTimes(2);
        });
    });

    describe('addToWatchlist method', () => {
        const director = new Director();
        director.firstName = 'Ivan';

        const movie = new Movie();
        movie.id = 1;
        movie.title = 'title';
        movie.director = director;
        movie.genres = [];
        movie.moviestars = [];

        it('should throw if movie is already in user\'s watchlist', async () => {
            // Arrange
            movieRepository.findOne = () => {
                return movie;
            };

            const role = new Role();
            role.roleName = '';

            const user = new User();
            user.watchlist = [movie];
            user.role = role;

            userRepository.findOne = () => {
                return user;
            };

            // Act & Assert
            let message = false;
            try {
                await moviesService.addToWatchlist(1, 1);
            } catch (err) {
                message = err.message;
            }
            expect(message).toBeTruthy();
        });

        it('should add movie into user\'s watchlist', async () => {
            // Arrange
            movieRepository.findOne = () => {
                return movie;
            };

            const role = new Role();
            role.roleName = '';

            const user = new User();
            user.watchlist = [];
            user.role = role;

            userRepository.findOne = () => {
                return user;
            };

            // Act
            await moviesService.addToWatchlist(1, 1);

            // Assert
            expect(user.watchlist[0]).toBeDefined();
        });
    });

    describe('addMovie method', () => {
        const director = new Director();
        director.id = 1;
        director.firstName = 'Ivan';

        const movie = new Movie();
        movie.id = 1;
        movie.title = 'title';
        movie.director = director;
        movie.genres = [];
        movie.moviestars = [];

        it('should call moviestarsRepository for each moviestarId provided', async () => {
            // Arrange
            const movieToAdd = new AddMovieDTO();
            movieToAdd.directorId = 1;
            movieToAdd.genres = [];
            movieToAdd.length = 1;
            movieToAdd.resume = '';
            movieToAdd.title = 'another title';
            movieToAdd.year = 1000;

            const moviestar1 = new Moviestar();
            moviestar1.id = 1;

            movieToAdd.moviestars = [1, 2, 3];

            movieRepository.findOne = () => {
                return null;
            };
            movieRepository.save = () => {
                return movie;
            };
            directorRepository.findOne = () => {
                return director;
            };
            moviestarRepository.findOne = () => {
                return {};
            };
            genresRepository.findOne = () => {
                return {};
            };

            const spy = jest.spyOn(moviestarRepository, 'findOne');

            // Act
            await moviesService.addMovie(movieToAdd);

            // Assert
            expect(spy).toBeCalledTimes(movieToAdd.moviestars.length);
        });
    });
});
