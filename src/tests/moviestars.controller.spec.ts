import { MoviestarsService } from './../common/core/moviestars.service';
import { MoviestarControler } from './../moviestars/moviestar.controller';
import { Test } from '@nestjs/testing';

describe('MoviestarController', () => {
  const moviestarService: MoviestarsService = new MoviestarsService(null);
  let moviestarCtrl: MoviestarControler;

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      controllers: [MoviestarControler],
      providers: [
        {
          provide: MoviestarsService,
          useValue: moviestarService,
        }],
    }).compile();

    moviestarCtrl = module.get<MoviestarControler>(MoviestarControler);
  });

  describe('method getMoviestar', () => {
    it('should get all moviestars if query is empty', async () => {
      // Arrange
      jest.spyOn(moviestarService, 'getAll').mockImplementation(() => {
        return {};
      });
      // Act
      await moviestarCtrl.getMoviestar('');
      // Assert
      expect(moviestarService.getAll).toHaveBeenCalled();
    });

    it('should get moviestar by first name if query.firstName', async () => {
      // Arrange
      jest.spyOn(moviestarService, 'getByFirstName').mockImplementation(() => {
        return {};
      });
      // Act
      await moviestarCtrl.getMoviestar({ firstName: 'John'});
      // Assert
      expect(moviestarService.getByFirstName).toHaveBeenCalled();
    });

    it('should get moviestar by first name if query.lastName', async () => {
      // Arrange
      jest.spyOn(moviestarService, 'getByLastName').mockImplementation(() => {
        return {};
      });
      // Act
      await moviestarCtrl.getMoviestar({ lastName: 'Snow'});
      // Assert
      expect(moviestarService.getByLastName).toHaveBeenCalled();
    });

  });

  it('should read a specific moviestar', async () => {
    // Arrange
    jest.spyOn(moviestarService, 'read').mockImplementation(() => {
      return {};
    });
    // Act
    await moviestarCtrl.readMoviestar(1);
    // Assert
    expect(moviestarService.read).toHaveBeenCalled();
    expect(moviestarService.read).toHaveBeenCalledWith(1);
  });

  it('should update a specific moviestar by ID', async () => {
    // Arrange
    const mockMoviestar = {
        firstName: 'Nicolle',
        lastName: 'Kiddman',
    };

    jest.spyOn(moviestarService, 'updateMoviestar').mockImplementation(() => {
      return mockMoviestar;
    });
    // Act
    const updatedMoviestar = {
        firstName: 'Nicole',
        lastName: 'Kiddman',
    };
    await moviestarCtrl.updateMoviestar(updatedMoviestar, 1);
    // Assert
    expect(moviestarService.updateMoviestar).toHaveBeenCalled();
    expect(moviestarService.updateMoviestar).toReturnWith(mockMoviestar);
  });

  it('should delete a specific moviestar by ID', async () => {
    // Arrange
    jest.spyOn(moviestarService, 'delete').mockImplementation(() => {
      return {};
    });
    // Act
    await moviestarCtrl.deleteMoviestar(1);
    // Assert
    expect(moviestarService.delete).toHaveBeenCalled();
  });
});