import { DirectorService } from './../common/core/director.service';
import { DirectorController } from './../directors/directors.controller';
import { Test } from '@nestjs/testing';

describe('DirectorController', () => {
  const directorService: DirectorService = new DirectorService(null);
  let directorCtrl: DirectorController;

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      controllers: [DirectorController],
      providers: [
        {
          provide: DirectorService,
          useValue: directorService,
        }],
    }).compile();

    directorCtrl = module.get<DirectorController>(DirectorController);
  });

  describe('method getDirectors', () => {
    it('should get all directors if query is empty', async () => {
      // Arrange
      jest.spyOn(directorService, 'getAll').mockImplementation(() => {
        return {};
      });
      // Act
      await directorCtrl.getDirectors('');
      // Assert
      expect(directorService.getAll).toHaveBeenCalled();
    });

    it('should get director by id if query.id', async () => {
      // Arrange
      jest.spyOn(directorService, 'read').mockImplementation(() => {
        return {};
      });
      // Act
      await directorCtrl.readDirector(1);
      // Assert
      expect(directorService.read).toHaveBeenCalled();
      expect(directorService.read).toHaveBeenCalledWith(1);
    });

    it('should get director by first name if query.firstName', async () => {
      // Arrange
      jest.spyOn(directorService, 'getByFirstName').mockImplementation(() => {
        return {};
      });
      // Act
      await directorCtrl.getDirectors({ firstName: 'Steven'});
      // Assert
      expect(directorService.getByFirstName).toHaveBeenCalled();
    });

    it('should get director by first name if query.lastName', async () => {
      // Arrange
      jest.spyOn(directorService, 'getByLastName').mockImplementation(() => {
        return {};
      });
      // Act
      await directorCtrl.getDirectors({ lastName: 'Spielberg'});
      // Assert
      expect(directorService.getByLastName).toHaveBeenCalled();
    });

  });

  it('should read a specific director', async () => {
    // Arrange
    jest.spyOn(directorService, 'read').mockImplementation(() => {
      return {};
    });
    // Act
    await directorCtrl.readDirector(1);
    // Assert
    expect(directorService.read).toHaveBeenCalled();
    expect(directorService.read).toHaveBeenCalledWith(1);
  });

  it('should update a specific director by ID', async () => {
    // Arrange
    const mockDirector = {
        firstName: 'Ivan',
        lastName: 'Ivanov',
    };

    jest.spyOn(directorService, 'updateDirector').mockImplementation(() => {
      return mockDirector;
    });
    // Act
    const updatedDirector = {
        firstName: 'Nicole',
        lastName: 'Kiddman',
        gender: 'female',
        networth: 30000,
    };
    await directorCtrl.updateDirector(1, updatedDirector);
    // Assert
    expect(directorService.updateDirector).toHaveBeenCalled();
    expect(directorService.updateDirector).toReturnWith(mockDirector);
  });

  it('should delete a specific moviestar by ID', async () => {
    // Arrange
    jest.spyOn(directorService, 'deleteDirector').mockImplementation(() => {
      return {};
    });
    // Act
    await directorCtrl.deleteDirector(1);
    // Assert
    expect(directorService.deleteDirector).toHaveBeenCalled();
  });
});