import { MoviestarsService } from './../common/core/moviestars.service';
import { Moviestar } from './../data/entities/moviestar.entity';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { AddMoviestarDTO } from '../models/user/add-moviestar.dto';
import { MoviestarRO } from '../models/moviestar.ro';

describe('MoviestarsService', () => {
    let moviestarsService: MoviestarsService;
    let movieEntity: Moviestar = new Moviestar();

    const moviestarRepository: any = {
        findOne: async () => {
            return movieEntity;
        },
        find: () => null,
        update: () => ({}),
        remove: () => ({}),
        save: async () => {
            return movieEntity;
        },
        create: () => ({}),
    };

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [MoviestarsService,
                        {
                            provide: 'MoviestarRepository',
                            useValue: moviestarRepository,
                        },
            ],
        })
/*             .overrideProvider(getRepositoryToken(Moviestar))
            .useValue(moviestarRepository) */
            .compile();

        moviestarsService = module.get<MoviestarsService>(MoviestarsService);
    });

    describe('read method ', () => {
        it('should return correct data', async () => {
            // Arrange
            movieEntity = new Moviestar();
            movieEntity.id = 1;
            movieEntity.firstName = 'Ivan';
            movieEntity.lastName = 'Ivanov';

            const moviestarRO: MoviestarRO = new MoviestarRO();
            moviestarRO.id = 1;
            moviestarRO.firstName = 'Ivan';
            moviestarRO.lastName = 'Ivanov';

            jest.spyOn(moviestarsService, 'read');

            // Act
            const result = await moviestarsService.read(1);

            // Assert
            expect(result.id).toBe(moviestarRO.id);
            expect(result.firstName).toBe(moviestarRO.firstName);
            expect(result.lastName).toBe(moviestarRO.lastName);
        });

        it('should throw error when moviestar is not found', async () => {
            // Arrange
            movieEntity = null;

            // Act & Assert
            let message = false;
            try {
                await moviestarsService.read(1);
            } catch (err) {
                message = err.message;
            }
            expect(message).toBeTruthy();
        });

        it('should call moviestarRepository findOne method', async () => {
            // Arrange
            movieEntity = new Moviestar();
            movieEntity.id = 3;
            movieEntity.firstName = 'Ivan';
            movieEntity.lastName = 'Ivanov';

            const spy = jest.spyOn(moviestarRepository, 'findOne');

            // Act
            await moviestarsService.read(1);

            // Assert
            expect(spy).toHaveBeenCalled();
        });
    });

    describe('addMoviestar method ', () => {
        let moviestarToAdd: AddMoviestarDTO;

        it('should save and return correct data', async () => {
            // Arrange
            moviestarToAdd = new AddMoviestarDTO();
            moviestarToAdd.firstName = 'Georgi';
            moviestarToAdd.lastName = 'Georgiev';

            movieEntity = new Moviestar();
            movieEntity.id = 1;
            movieEntity.firstName = 'Georgi';
            movieEntity.lastName = 'Georgiev';

            moviestarRepository.findOne = () => {
                return null;
            };

            jest.spyOn(moviestarsService, 'addMoviestar');

            // Act
            const result = await moviestarsService.addMoviestar(moviestarToAdd);

            // Assert
            expect(result.firstName).toBe(moviestarToAdd.firstName);
            expect(result.lastName).toBe(moviestarToAdd.lastName);
        });

        it('should throw error when moviestar already exists', async () => {
            // Arrange
            moviestarToAdd = new AddMoviestarDTO();
            moviestarToAdd.firstName = 'Georgi';
            moviestarToAdd.lastName = 'Georgiev';

            movieEntity = new Moviestar();
            movieEntity.id = 1;
            movieEntity.firstName = 'Georgi';
            movieEntity.lastName = 'Georgiev';

            moviestarRepository.findOne = () => {
                return movieEntity;
            };

            jest.spyOn(moviestarsService, 'addMoviestar');

            // Act & Assert
            let message = false;
            try {
                await moviestarsService.addMoviestar(moviestarToAdd);
            } catch (err) {
                message = err.message;
            }
            expect(message).toBeTruthy();
        });

        it('should call moviestarRepository findOne method', async () => {
            // Arrange
            moviestarToAdd = new AddMoviestarDTO();
            moviestarToAdd.firstName = 'Georgi';
            moviestarToAdd.lastName = 'Georgiev';

            moviestarRepository.findOne = () => {
                return null;
            };

            const spy = jest.spyOn(moviestarRepository, 'findOne');

            // Act
            await moviestarsService.addMoviestar(moviestarToAdd);

            // Assert
            expect(spy).toHaveBeenCalled();
        });

        it('should call moviestarRepository create method', async () => {
            // Arrange
            moviestarToAdd = new AddMoviestarDTO();
            moviestarToAdd.firstName = 'Georgi';
            moviestarToAdd.lastName = 'Georgiev';

            moviestarRepository.findOne = () => {
                return null;
            };

            const spy = jest.spyOn(moviestarRepository, 'create');

            // Act
            await moviestarsService.addMoviestar(moviestarToAdd);

            // Assert
            expect(spy).toHaveBeenCalled();
        });

        it('should call moviestarRepository save method', async () => {
            // Arrange
            moviestarToAdd = new AddMoviestarDTO();
            moviestarToAdd.firstName = 'Georgi';
            moviestarToAdd.lastName = 'Georgiev';

            moviestarRepository.findOne = () => {
                return null;
            };

            const spy = jest.spyOn(moviestarRepository, 'save');

            // Act
            await moviestarsService.addMoviestar(moviestarToAdd);

            // Assert
            expect(spy).toHaveBeenCalled();
        });
    });

    describe('updateMoviestar method ', () => {
        it('should update and return correct data', async () => {
            // Arrange
            const updatingMoviestar: Partial<AddMoviestarDTO> = new AddMoviestarDTO();
            updatingMoviestar.firstName = 'Ivan';

            movieEntity = new Moviestar();
            movieEntity.id = 1;
            movieEntity.firstName = 'Georgi';
            movieEntity.lastName = 'Georgiev';

            const updatedMoviestar = new Moviestar();
            updatedMoviestar.id = movieEntity.id;
            updatedMoviestar.firstName = 'Ivan';
            updatedMoviestar.lastName = movieEntity.lastName;

            moviestarRepository.findOne = () => {
                return movieEntity;
            };
            moviestarRepository.update = () => {
                moviestarRepository.findOne = () => {
                    return updatedMoviestar;
                };

                return updatedMoviestar;
            };

            // Act
            const result = await moviestarsService.updateMoviestar(1, updatingMoviestar);

            // Assert
            expect(result.id).toBe(updatedMoviestar.id);
            expect(result.firstName).toBe(updatedMoviestar.firstName);
            expect(result.lastName).toBe(updatedMoviestar.lastName);
        });

        it('should call moviestarRepository findOne method twice', async () => {
            // Arrange
            const updatingMoviestar: Partial<AddMoviestarDTO> = new AddMoviestarDTO();
            updatingMoviestar.firstName = 'Ivan';

            movieEntity = new Moviestar();
            movieEntity.id = 1;
            movieEntity.firstName = 'Georgi';
            movieEntity.lastName = 'Georgiev';

            moviestarRepository.findOne = () => {
                return movieEntity;
            };
            moviestarRepository.update = () => {
                return movieEntity;
            };

            const spy = jest.spyOn(moviestarRepository, 'findOne');

            // Act
            await moviestarsService.updateMoviestar(1, updatingMoviestar);

            // Assert
            expect(spy).toHaveBeenCalledTimes(2);
        });
    });

    describe('getByFirstName method', () => {
        it('should return moviestars found', async () => {
            // Arrange
            const query: string = 'Ivan';

            movieEntity = new Moviestar();
            movieEntity.id = 1;
            movieEntity.firstName = 'Ivan';
            movieEntity.lastName = 'Ivanov';

            moviestarRepository.find = () => {
                return [movieEntity];
            };

            // Act & Assert
            expect(await moviestarsService.getByFirstName(query)).toEqual([movieEntity]);
        });
    });
});
