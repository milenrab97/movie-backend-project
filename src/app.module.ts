import { RoleModule } from './role/role.module';
import { GenreModule } from './genre/genre.module';
import { DirectorModule } from './directors/directors.module';
import { Module, HttpModule } from '@nestjs/common';
import { ConfigModule } from './config/config.module';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { CoreModule } from './common/core/core.module';
import { DatabaseModule } from './database/database.module';
import { MoviesModule } from './movies/movies.module';
import { MoviestarModule } from './moviestars/moviestar.module';
import { APP_FILTER } from '@nestjs/core';
import { MainHttpExceptionFilter } from './filters/main-http-exception.filter';

@Module({
  imports: [
    ConfigModule,
    HttpModule,
    AuthModule,
    DatabaseModule,
    UsersModule,
    CoreModule,
    DatabaseModule,
    MoviesModule,
    DirectorModule,
    MoviestarModule,
    GenreModule,
    RoleModule,
  ],
  controllers: [],
  providers: [
    {
      provide: APP_FILTER,
      useClass: MainHttpExceptionFilter,
    },
  ],
})
export class AppModule { }
