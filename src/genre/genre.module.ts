import { AuthModule } from './../auth/auth.module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CoreModule } from 'src/common/core/core.module';
import { GenreController } from './genre.controller';
import { Genre } from '../data/entities/genre.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Genre]), CoreModule, AuthModule],
  providers: [],
  exports: [],
  controllers: [GenreController],
})
export class GenreModule { }