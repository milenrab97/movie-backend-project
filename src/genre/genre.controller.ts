import { AddGenreDTO } from './../models/user/add-genre.dto';
import { GenreService } from '../common/core/genre.service';
import { Controller, Get, Query, Post, UseGuards, Body, ValidationPipe, Put, Param, ParseIntPipe, Delete } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Roles, RolesGuard } from '../common';

@Controller('genre')
export class GenreController {
    constructor(
        private readonly genreService: GenreService,
    ) { }
    @Get()
    getAllGenres(@Query() query) {
        return this.genreService.getAll();
    }
    @Get(':id')
    readGenre(@Param('id', new ParseIntPipe()) id: number) {
        return this.genreService.getByID(id);
    }

    @Post()
    @Roles('admin')
    @UseGuards(AuthGuard(), RolesGuard)
    addGenre(@Body(new ValidationPipe({
        transform: true,
        whitelist: true,
    }))
    genre: AddGenreDTO) {
        return this.genreService.addGenre(genre);
    }

    @Put(':id')
    @Roles('admin')
    @UseGuards(AuthGuard(), RolesGuard)

    updateGenre(
        @Body(new ValidationPipe({
            transform: true,
            whitelist: true,
            skipMissingProperties: true,
        })) genre: AddGenreDTO,
        @Param('id', new ParseIntPipe()) id: number,
    ) {
        return this.genreService.updateGenre(id, genre);
    }

    @Delete(':id')
    @Roles('admin')
    @UseGuards(AuthGuard(), RolesGuard)
    deleteGenre(
        @Param('id', new ParseIntPipe()) id: number,
    ) {
        return this.genreService.deleteGenre(id);
    }
}
