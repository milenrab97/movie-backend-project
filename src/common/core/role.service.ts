import { AddRoleDTO } from './../../models/user/add-role.dto';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { Role } from '../../data/entities/role.entity';

@Injectable()
export class RoleService {
  constructor(
    @InjectRepository(Role)
    private readonly roleRepository: Repository<Role>,

  ) { }

  async addRole(role: AddRoleDTO) {
    const roleFound = await this.roleRepository.findOne({ where: { roleName: role.roleName } });

    if (roleFound) {
      throw new Error('Role already exists!');
    }
    await this.roleRepository.create(role);

    const addedRole = await this.roleRepository.save(role);

    return addedRole;
  }

  async getAll() {
    return this.roleRepository.find({});
  }
  async getByID(id: number) {
    return await this.roleRepository.findOne({ where: { id } });
  }

  async updateRole(id: number, role: AddRoleDTO) {
    const roleInDB = await this.roleRepository.findOne({ where: { id } });

    if (!roleInDB) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    await this.roleRepository.update(id, role);

    const updatedRole = await this.roleRepository.findOne({ where: { id } });

    return updatedRole;
  }

  async delete(id: number) {
    const role = await this.roleRepository.findOne({ where: { id } });

    if (!role) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    await this.roleRepository.remove(role);

    return role;
  }
}