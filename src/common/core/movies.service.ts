import { MovieRO } from './../../models/movie.ro';
import { Moviestar } from './../../data/entities/moviestar.entity';
import { AddMovieDTO } from './../../models/user/add-movie.dto';
import { MovieDTO } from './../../models/user/movie.dto';
import { GetUserDTO } from '../../models/user/get-user.dto';
import { UserLoginDTO } from '../../models/user/user-login.dto';
import { UserRegisterDTO } from '../../models/user/user-register.dto';
import { Injectable, NotFoundException, BadRequestException, HttpException, HttpStatus } from '@nestjs/common';
import { Repository, TransactionManager, EntityManager, Transaction, Like, Equal, getConnection, getTreeRepository, RelationId } from 'typeorm';
import { User } from './../../data/entities/user.entity';
import { InjectRepository, InjectEntityManager } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { JwtPayload } from './../../interfaces/jwt-payload';
import { validate, Equals } from 'class-validator';
import { Director } from './../../data/entities/director.entity';
import { moveCursor } from 'readline';
import { Vote } from './../../data/entities/vote.entity';
import { Genre } from './../../data/entities/genre.entity';
import { UserRO } from 'src/models/user/user.ro';
import { Movie } from './../../data/entities/movie.entity';

@Injectable()
export class MoviesService {
    public readonly MOVIES_PER_PAGE = 2;
    constructor(
        @InjectRepository(Movie)
        private readonly movieRepository: Repository<Movie>,
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
        @InjectRepository(Director)
        private readonly directorsRepository: Repository<Director>,
        @InjectRepository(Vote)
        private readonly votesRepository: Repository<Vote>,
        @InjectRepository(Moviestar)
        private readonly moviestarsRepository: Repository<Moviestar>,
        @InjectRepository(Genre)
        private readonly genresRepository: Repository<Genre>,

    ) { }
    async getAll(page: number = 1, count: number): Promise<MovieRO[]> {
        count = count ? count : this.MOVIES_PER_PAGE;
        const movies = await this.movieRepository.find({
            take: count,
            skip: (page - 1) * count,
        });

        const moviesRO: MovieRO[] = movies.map((movie) => this.convertToRO(movie));

        return moviesRO;
    }

    async read(id: number): Promise<MovieRO> {
        const movie = await this.movieRepository.findOne({
            where: { id },
        });

        if (!movie) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }

        return this.convertToRO(movie);
    }

    async getByTitle(page: number = 1, count: number, searchTitle: string): Promise<MovieRO[]> {
        count = count ? count : this.MOVIES_PER_PAGE;
        const movies = await this.movieRepository.find({
            take: count,
            skip: (page - 1) * count,
            where: {
                title: Like(`%${searchTitle}%`),
            },
        });

        return movies.map((movie) => this.convertToRO(movie));
    }

    async getWatchlist(userId: number) {
        const userFound = await this.userRepository.findOne({ where: { id: userId } });

        if (!userFound) {
            throw new HttpException('User not found', HttpStatus.NOT_FOUND);
        }

        return userFound.watchlist.map((movie) => this.convertToRO(movie));
    }

    async addMovie(movie: AddMovieDTO): Promise<MovieRO> {
        const movieInDB = await this.movieRepository.findOne({
            where: { title: movie.title },
        });

        if (movieInDB) {
            throw new HttpException('Movie already exists!', HttpStatus.BAD_REQUEST);
        }

        const movieToAdd = new Movie();

        movieToAdd.title = movie.title;
        movieToAdd.year = movie.year;
        movieToAdd.length = movie.length;
        movieToAdd.resume = movie.resume;
        movieToAdd.director = await this.directorsRepository.findOne({ where: { id: movie.directorId } });

        const actors = await Promise.all(movie.moviestars.map(async (star) => {
            const moviestarInDB = await this.moviestarsRepository.findOne({ id: star });
            if (!moviestarInDB) {
                throw new HttpException('Bad request', HttpStatus.BAD_REQUEST);
            }
            return moviestarInDB;
        }));
        movieToAdd.moviestars = actors;

        const genres = await Promise.all(movie.genres.map(async (genre) => {
            const genreInDb = await this.genresRepository.findOne({ where: { id: genre } });
            if (!genreInDb) {
                throw new HttpException('Bad request', HttpStatus.BAD_REQUEST);
            }
            return genreInDb;
        }));
        movieToAdd.genres = genres;

        await this.movieRepository.create(movieToAdd);

        const res = await this.movieRepository.save(movieToAdd);
        return this.convertToRO(res);
    }

    async updateMovie(id: number, newInfo: Partial<AddMovieDTO>): Promise<MovieRO> {
        const movieInDB = await this.movieRepository.findOne({ where: { id } });

        if (!movieInDB) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }

        movieInDB.title = newInfo.title;
        movieInDB.year = newInfo.year;
        movieInDB.length = newInfo.length;
        movieInDB.resume = newInfo.resume;

        const director = await this.directorsRepository.findOne({ where: { id: newInfo.directorId } });
        if (!director) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        movieInDB.director = director;

        if (newInfo.moviestars) {
            const actors = await Promise.all(newInfo.moviestars.map(async (star) => {
                const moviestarInDB = await this.moviestarsRepository.findOne({ id: star });
                if (!moviestarInDB) {
                    throw new HttpException('Bad request', HttpStatus.BAD_REQUEST);
                }
                return moviestarInDB;
            }));
            movieInDB.moviestars = actors;
        }

        if (newInfo.genres) {
            const genres = await Promise.all(newInfo.genres.map(async (genre) => {
                const genreInDb = await this.genresRepository.findOne({ where: { id: genre } });
                if (!genreInDb) {
                    throw new HttpException('Bad request', HttpStatus.BAD_REQUEST);
                }
                return genreInDb;
            }));
            movieInDB.genres = genres;
        }

        await this.movieRepository.save(movieInDB);

        const updatedMovie = await this.movieRepository.findOne({
            where: { id },
        });
        return this.convertToRO(updatedMovie);
    }

    async deleteMovie(id: number): Promise<MovieRO> {
        const movie = await this.movieRepository.findOne({
            where: { id },
        });
        if (!movie) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }

        await this.movieRepository.remove(movie);

        return this.convertToRO(movie);
    }

    async addToWatchlist(id: number, userId: number): Promise<UserRO> {
        const movie = await this.movieRepository.findOne({ where: { id } });
        const user = await this.userRepository.findOne({
            where: { id: userId },
        });

        if (user.watchlist.filter(m => m.id === movie.id).length > 0) {
            throw new HttpException('Movie already in watchlist!', HttpStatus.BAD_REQUEST);
        } else {
            user.watchlist.push(movie);
            await this.userRepository.save(user);
        }

        return user.convertToRO();
    }

    async removeFromWatchlist(id: number, userId: number): Promise<UserRO> {
        const movie = await this.movieRepository.findOne({ where: { id } });
        const user = await this.userRepository.findOne({ where: { id: userId } });

        if (!movie || user.watchlist.length === 0) {
            throw new Error('No movies in watchlist!');
        }

        const indexOfMovieInWatchlist = user.watchlist.findIndex((m: Movie) => {
            return m.id === movie.id;
        });

        if (indexOfMovieInWatchlist === -1) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        } else {
            user.watchlist = user.watchlist.filter(m => {
                return m.id !== movie.id;
            });
        }

        await this.userRepository.save(user);

        return user.convertToRO();
    }

    async rateMovie(id: number, userId: number, rating: number): Promise<MovieRO> {
        const movie = await this.movieRepository.findOne({ where: { id } });
        const user = await this.userRepository.findOne({ where: { id: userId } });

        if (!movie) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }

        const voteInDB = await this.votesRepository.findOne({
            where: { movie, user },
        });

        if (voteInDB) {
            voteInDB.rating = rating;
            await this.votesRepository.update({ id: voteInDB.id }, { rating });
        } else {
            const vote = new Vote();
            vote.rating = rating;
            vote.user = user;
            vote.movie = movie;

            await this.votesRepository.create(vote);
            await this.votesRepository.save([vote]);
        }

        const ratedMovie: Movie = await this.movieRepository.findOne({ where: { id } });

        return this.convertToRO(ratedMovie);
    }

    private convertToRO(movie: Movie): MovieRO {
        const movieRO: MovieRO = new MovieRO();

        movieRO.id = movie.id;
        movieRO.title = movie.title;
        movieRO.year = movie.year;
        movieRO.length = movie.length;
        movieRO.resume = movie.resume;
        movieRO.director = movie.director.convertToRO();
        movieRO.rating = this.getMovieRating(movie);
        movieRO.genres = movie.genres.map((m) => m.genreName);
        movieRO.moviestars = movie.moviestars.map((m) => m.convertToRO(false));

        /*         const movieRO: any = {
                    ...movie,
                    rating: this.getMovieRating(movie),
                }; */

        return movieRO;
    }

    private getMovieRating(movie: Movie): number {
        if (!movie.votes) {
            return 0;
        }
        return Number((movie.votes.reduce((acc, vote) => acc + vote.rating, 0) / movie.votes.length).toFixed(2));
    }
}
