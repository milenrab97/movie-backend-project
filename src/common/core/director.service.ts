import { DirectorRO } from 'src/models/director.ro';
import { AddDirectorDTO } from './../../models/user/add-director.dto';
import { Director } from './../../data/entities/director.entity';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Like } from 'typeorm';

@Injectable()
export class DirectorService {
  constructor(
    @InjectRepository(Director)
    private readonly directorRepository: Repository<Director>,

  ) { }

  async addDirector(director: AddDirectorDTO): Promise<DirectorRO> {
    const directorFound = await this.directorRepository.findOne({
      where: { firstName: director.firstName, lastName: director.lastName },
    });

    if (directorFound) {
      throw new HttpException('Director already exists', HttpStatus.BAD_REQUEST);
    }

    const directorToAdd = new Director();

    directorToAdd.firstName = director.firstName;
    directorToAdd.lastName = director.lastName;
    directorToAdd.gender = director.gender;
    directorToAdd.networth = director.networth;

    await this.directorRepository.create(directorToAdd);

    const result: Director = await this.directorRepository.save(directorToAdd);

    return result.convertToRO();
  }

  async updateDirector(id: number, newInfo: Partial<AddDirectorDTO>): Promise<DirectorRO> {
    const directorInDB = await this.directorRepository.findOne({
      where: { id },
    });

    if (!directorInDB) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    await this.directorRepository.update({id}, newInfo);

    const updatedDirector = await this.directorRepository.findOne({
      where: { id },
    });

    return updatedDirector.convertToRO();
  }

  async getAll(): Promise<DirectorRO[]> {
    const directors = await this.directorRepository.find({});

    const directorsRO: DirectorRO[] = directors.map((director) => director.convertToRO());

    return directorsRO;
  }

  async read(id: number): Promise<DirectorRO> {
    const director = await this.directorRepository.findOne({
      where: { id },
    });

    if (!director) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    return director.convertToRO();
  }

  async getByFirstName(firstName: string): Promise<DirectorRO[]> {
    const directors = await this.directorRepository.find({
      firstName: Like(`%${firstName}%`),
    });

    const directorsRO: DirectorRO[] = directors.map((director) => director.convertToRO());

    return directorsRO;
  }
  async getByLastName(lastName: string): Promise<DirectorRO[]> {
    const directors = await this.directorRepository.find({
      lastName: Like(`%${lastName}%`),
    });

    const directorsRO: DirectorRO[] = directors.map((director) => director.convertToRO());

    return directorsRO;
  }

  async deleteDirector(id: number): Promise<DirectorRO> {
    const director = await this.directorRepository.findOne({
      where: { id },
    });

    if (!director) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    await this.directorRepository.remove(director);

    return director.convertToRO();
  }
}
