import { AddMoviestarDTO } from './../../models/user/add-moviestar.dto';
import { Moviestar } from './../../data/entities/moviestar.entity';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Like } from 'typeorm';
import { MoviestarRO } from 'src/models/moviestar.ro';

@Injectable()
export class MoviestarsService {
  constructor(
    @InjectRepository(Moviestar)
    private readonly moviestarRepository: Repository<Moviestar>,

  ) { }

  async addMoviestar(moviestar: AddMoviestarDTO) {
    const moviestarFound = await this.moviestarRepository.findOne({ where: { firstName: moviestar.firstName, lastName: moviestar.lastName } });

    if (moviestarFound) {
      throw new HttpException('Moviestar exists!', HttpStatus.BAD_REQUEST);
    }

    const moviestarToAdd = new Moviestar();

    moviestarToAdd.firstName = moviestar.firstName;
    moviestarToAdd.lastName = moviestar.lastName;

    await this.moviestarRepository.create(moviestarToAdd);

    const result = await this.moviestarRepository.save(moviestarToAdd);

    return result.convertToRO();
  }

  async updateMoviestar(id: number, moviestar: Partial<AddMoviestarDTO>) {
    const moviestarInDB = await this.moviestarRepository.findOne({ where: { id } });

    if (!moviestarInDB) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    await this.moviestarRepository.update({ id }, moviestar);

    const updatedMoviestar = await this.moviestarRepository.findOne({ where: { id } });

    return updatedMoviestar.convertToRO();
  }

  async getAll(): Promise<MoviestarRO[]> {
    const moviestars = await this.moviestarRepository.find({ relations: ['movies'] });

    return moviestars.map((moviestar) => moviestar.convertToRO());
  }
  async getByFirstName(query: string) {
    return await this.moviestarRepository.find({
      firstName: Like(`%${query}%`),
    });
  }
  async getByLastName(query: string) {
    return await this.moviestarRepository.find({
      lastName: Like(`%${query}%`),
    });
  }

  async read(id: number): Promise<MoviestarRO> {
    const moviestar = await this.moviestarRepository.findOne({ where: { id } });

    if (!moviestar) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    return moviestar.convertToRO();
  }
  async delete(id: number): Promise<MoviestarRO> {
    const moviestar = await this.moviestarRepository.findOne({ where: { id } });

    if (!moviestar) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    await this.moviestarRepository.remove(moviestar);

    return moviestar.convertToRO();
  }
}
