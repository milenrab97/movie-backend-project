import { RoleService } from './role.service';
import { GenreService } from './genre.service';
import { MoviestarsService } from './moviestars.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersService } from './users.service';
import { Module } from '@nestjs/common';
import { User } from './../../data/entities/user.entity';
import { FileService } from './file.service';
import { MoviesService } from './movies.service';
import { Movie } from 'src/data/entities/movie.entity';
import { Director } from '../../data/entities/director.entity';
import { DirectorService } from './director.service';
import { Moviestar } from '../../data/entities/moviestar.entity';
import { Role } from 'src/data/entities/role.entity';
import { Vote } from 'src/data/entities/vote.entity';
import { Genre } from 'src/data/entities/genre.entity';

@Module({
  imports: [TypeOrmModule.forFeature([
    User,
    Movie,
    Director,
    Moviestar,
    Role, Vote,
    Genre,
    Role])],

  providers: [
    UsersService,
    FileService,
    MoviesService,
    DirectorService,
    MoviestarsService,
    GenreService,
    RoleService],

  exports: [UsersService,
    FileService,
    MoviesService,
    DirectorService,
    MoviestarsService,
    GenreService,
    RoleService],
})
export class CoreModule { }
