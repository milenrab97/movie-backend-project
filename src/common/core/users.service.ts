import { UserLoginDTO } from '../../models/user/user-login.dto';
import { UserRegisterDTO } from '../../models/user/user-register.dto';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { Repository } from 'typeorm';
import { User } from './../../data/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { JwtPayload } from './../../interfaces/jwt-payload';
import { Role } from '../../data/entities/role.entity';
import { UserRO } from 'src/models/user/user.ro';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
    @InjectRepository(Role)
    private readonly rolesRepository: Repository<Role>,

  ) { }

  async registerUser(user: UserRegisterDTO) {
    const userFound = await this.usersRepository.findOne({ where: { email: user.email } });

    const userToAdd = new User();
    if (userFound) {
      throw new HttpException('User already exists!', HttpStatus.BAD_REQUEST);
    }

    userToAdd.email = user.email;
    userToAdd.password = await bcrypt.hash(user.password, 10);
    userToAdd.role = await this.rolesRepository.findOne({ where: { roleName: 'user' } });

    await this.usersRepository.create(userToAdd);

    const result = await this.usersRepository.save([userToAdd]);

    return result;
  }

  async validateUser(payload: JwtPayload): Promise<User> {
    const userFound = await this.usersRepository.findOne({ where: { id: payload.id } });
    return userFound;
  }

  async signIn(user: UserLoginDTO): Promise<User> {
    // const userFound: GetUserDTO = await this.usersRepository.findOne({ select: ['email', 'isAdmin', 'password'], where: { email: user.email } });
    const userFound = await this.usersRepository.findOne({ where: { email: user.email } });

    if (userFound) {
      const result = await bcrypt.compare(user.password, userFound.password);
      if (result) {
        return userFound;
      }
    }

    return null;
  }

  async getAll() {
    return this.usersRepository.find({});
  }

  async read(id: number, useRO = false) {
    const userFound = await this.usersRepository.findOne({ where: { id } });

    if (!userFound) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    if (useRO) {
      return userFound;
    }

    const userRO = new UserRO();
    userRO.id = userFound.id;
    userRO.email = userFound.email;
    userRO.role = userFound.role.roleName;

    return userRO;
  }
  
  async promoteUser(userId: number) {
    const userInDB = await this.usersRepository.findOne({ where: { id: userId } });

    if (!userInDB) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    const adminRole = await this.rolesRepository.findOne({ where: { roleName: 'admin' } });

    userInDB.role = adminRole;

    await this.usersRepository.save(userInDB);

    return 'User successfully promoted to admin!';
  }
}
