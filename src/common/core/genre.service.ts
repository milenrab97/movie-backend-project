import { AddGenreDTO } from './../../models/user/add-genre.dto';
import { Genre } from '../../data/entities/genre.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { GenreRO } from '../../models/genre.ro';

@Injectable()
export class GenreService {
  constructor(
    @InjectRepository(Genre)
    private readonly genreRepository: Repository<Genre>,

  ) { }

  async addGenre(genre: AddGenreDTO): Promise<GenreRO> {
    const genreFound = await this.genreRepository.findOne({ where: { genreName: genre.genreName } });

    // console.log(directorFound);
    if (genreFound) {
      throw new HttpException('Genre already exists!', HttpStatus.BAD_REQUEST);
    }

    const genreToAdd = new Genre();
    genreToAdd.genreName = genre.genreName;

    await this.genreRepository.create(genreToAdd);

    const result = await this.genreRepository.save(genreToAdd);

    return result.convertToRO();
  }

  async getAll(): Promise<GenreRO[]> {
    const genres = await this.genreRepository.find({});

    const genresRO: GenreRO[] = genres.map((genre) => genre.convertToRO());

    return genresRO;
  }
  async getByID(id: number): Promise<GenreRO> {
    const genre = await this.genreRepository.findOne({ where: { id } });

    if (!genre) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    return genre.convertToRO();
  }

  async updateGenre(id: number, genre: AddGenreDTO): Promise<GenreRO> {
    const genreInDB = await this.genreRepository.findOne({ where: { id } });

    if (!genreInDB) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    await this.genreRepository.update(id, genre);

    const updatedGenre = await this.genreRepository.findOne({ where: { id } });

    return updatedGenre.convertToRO();
  }

  async deleteGenre(id: number): Promise<GenreRO> {
    const genre = await this.genreRepository.findOne({ where: { id } });
    console.log(genre);
    if (!genre) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    await this.genreRepository.remove(genre);

    return genre.convertToRO();
  }
}
